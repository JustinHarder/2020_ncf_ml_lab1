import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.datasets import fetch_openml

"""
The base version of this code was copied directly from https://towardsdatascience.com/logistic-regression-using-python-sklearn-numpy-mnist-handwriting-recognition-matplotlib-a6b31e2b166a
in accordance with the instructions for this Lab.

"""

# Change data_home to wherever to where you want to download your data
mnist = fetch_openml('mnist_784', version=1)

# Print to show there are 1797 images (8 by 8 images for a dimensionality of 64)
print("Image Data Shape", mnist.data.shape)

# Print to show there are 1797 labels (integers from 0-9)
print("Label Data Shape", mnist.target.shape)

def NewFeature(input):
    copy = input.copy()
    def inversion_counter(array):
        increasing = True
        flips = 0
        for i in range(len(array) - 1):
            if increasing:
                if array[i] > array[i + 1]:
                    flips += 1
                    increasing = False
            else:
                if array[i] < array[i + 1]:
                    increasing = True
        return flips
    '''
    This for loop does the following:
    
    Segments the image into rows.
    Returns the total number of zeros in a row and the sum of a row.
    
    These features of an image created marginal improvements. 
    '''
    for i in range(28):
        row = input[28*i:(28*(i+1)-1)]
        sum_of_a_row = sum(row)
        number_of_zeros = sum(list(map(lambda x: 1 if x == 0 else 0, row)))
        conversion = list(map(lambda x: 0 if x == 0 else 1, row))
        #Here we count the number of gaps in a row.
        counting = []
        start = False
        count = 0
        for i in range(len(conversion) - 1):
            dope = conversion[i:i + 2]
            if start:
                if sum(dope) == 1:
                    count += 1
                    start = False
            else:
                if sum(dope) == 2:
                    start = True
        if conversion[26] == 1:
            count = count
        else:
            count = count-1
        counting.append(count)
        #sum_of_a_row, number_of_zeros,
        copy = np.append(copy,[sum_of_a_row, number_of_zeros])
    #Here we are going to count the number of inversions (increasing to decreasing) along
    # the vector which is the number of inversions in the rows. The idea being that 8 has a
    # vector of row inversions looking like [0,1,2,2,2,1,1,2,2,2,1,0]. This vector has 2 inversions.
    copy = np.append(copy, counting)
    flips = inversion_counter(counting)
    copy = np.append(copy, [flips])
    return copy

mnist.data = np.array(list(map(NewFeature, mnist.data)))



from sklearn.model_selection import train_test_split
train_img, test_img, train_lbl, test_lbl = train_test_split(
    mnist.data, mnist.target, test_size=1/7.0, random_state=0)

print(train_img.shape)
print(train_lbl.shape)
print(test_img.shape)
print(test_lbl.shape)








plt.figure(figsize=(20,4))

for index, (image, label) in enumerate(zip(train_img[0:5], train_lbl[0:5])):
    image = image[0:784]
    plt.subplot(1, 5, index + 1)
    plt.imshow(np.reshape(image, (28,28)), cmap=plt.cm.gray)
    plt.title('Training: %i\n' % int(label), fontsize = 20)



# all parameters not specified are set to their defaults
# default solver is incredibly slow thats why we change it
logisticRegr = LogisticRegression(solver = 'lbfgs')

logisticRegr.fit(train_img, train_lbl)

# Returns a NumPy Array
# Predict for One Observation (image)
logisticRegr.predict(test_img[0].reshape(1,-1))

# Predict for Multiple Observations (images) at Once
logisticRegr.predict(test_img[0:10])

# Make predictions on entire test data
predictions = logisticRegr.predict(test_img)

score = logisticRegr.score(test_img, test_lbl)
print(score)

index = 0
misclassifiedIndexes = []
for label, predict in zip(test_lbl, predictions):
    if label != predict:
        misclassifiedIndexes.append(index)
    index +=1

plt.figure(figsize=(20,4))
for plotIndex, badIndex in enumerate(misclassifiedIndexes[0:5]):
    plt.subplot(1, 5, plotIndex + 1)
    plt.imshow(np.reshape(test_img[badIndex][0:784], (28,28)), cmap=plt.cm.gray)
    plt.title('Predicted: {}, Actual: {}'.format(predictions[badIndex], test_lbl[badIndex]), fontsize = 15)


import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics

cm = metrics.confusion_matrix(test_lbl, predictions)

plt.figure(figsize=(9,9))
plt.imshow(cm, interpolation='nearest', cmap='Pastel1')
plt.title('Confusion matrix', size = 15)
plt.colorbar()
tick_marks = np.arange(10)
plt.xticks(tick_marks, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"], rotation=45, size = 10)
plt.yticks(tick_marks, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"], size = 10)
plt.tight_layout()
plt.ylabel('Actual label', size = 15)
plt.xlabel('Predicted label', size = 15)
width, height = cm.shape
for x in range(width):
    for y in range(height):
         plt.annotate(str(cm[x][y]), xy=(y, x),
         horizontalalignment='center',
         verticalalignment='center')

plt.show()