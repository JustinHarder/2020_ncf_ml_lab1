# README #

0) Fork this repository to one accessible to you, your partner, Daniel Xie (dxie@ncf.edu), and me. (To fork, click + on the left and choose Fork.)

1) 

* Create a file named mnist.py. Follow the example on the page here (https://towardsdatascience.com/logistic-regression-using-python-sklearn-numpy-mnist-handwriting-recognition-matplotlib-a6b31e2b166a). I.e., mostly copy and paste for this part. 
* NOTE!!!: the page considers two examples, the first with "digits", a smaller database with less resolution, the second is MNIST.
* Note, you should add "plt.show()" as the last plt command to display the plots that should be the same as on the web page.
* Once you have that reproduced, commit mnist.py to your own branch.

2) 

* Create a file named answers.txt. Did you find a bug in the code (because you were not getting the same output as on the webpage?
* If yes, explain what it was in answers.txt. If you did get exactly the same output as on the webpage, don't worry about this part. Commit.

3) Which optimization method is substituted for the default one here? Why does this one work better than the default? Commit your answer to answers.txt

4) What is the difference between the two optimization methods above and the methods in the plot included in the "explanation"?

4) ex) (This one is extra credit, don't worry if it does not come to you, but at least think about it.) What is "weird" about the paths for the class of optimization methods that are being demonstrated in the animation? 

5) Include the confusion matrix code from the "digits" example in your MNIST code. Commit the change.

6) What do the columns and rows mean in the confusion matrix? Which numbers are being confused for an 8 and at what frequencies? Which numbers is 8 being confused with and at what frequencies?

above due by 11:59 pm 2/28

7) Try to come up with extra features that would make detection of 8 better. Submit this and the appropriate code on your branch.

8) Does doing the above also improve the detection of other numbers that used to be confused for an 8?

above due by 11:59 pm 3/4

Note, you can get >90% accuracy with just a simple logistic regression on the pixels themselves as they are.

Answers to non-coding parts should be in answers.txt 

Commit your code and answers so that it will be easy for Daniel to find what is what.

If it is not on Bitbucket it was not done.

---------------------------

* Groups:

1) Ashley
1) Justin

2) Jaida
2) Amanda

3) Rosa
3) Naeem

4) Jamie
4) Nate

5) Simona
5) Andrew

6) Olivia
6) Dominic
6) Atalay

---------------------------

You will be evaluated on all of the following:

a) The presentation (organization, readability, clarity, timeliness of submissions, etc.)

b) How thorough and complete the work is. 

c) Correctness. 

