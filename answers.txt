2) We did find a bug in the code, as after running we first got an error
LINE 12

We found out that loading the mnist dataset was a deprecated, so we replaced this line:
>> from sklearn.datasets import fetch_mldata
mnist = fetch_mldata('MNIST original')
that gave this error:
TimeoutError: [WinError 10060] A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond

with this line:
>> from sklearn.datasets import fetch_openml
   # Load data from https://www.openml.org/d/554
   X, y = fetch_openml('mnist_784', version=1, return_X_y=True)

Which we found on this stack overflow page:
https://stackoverflow.com/questions/53096977/mnist-data-download-from-sklearn-datasets-gives-timeout-error

LINE 35
There was an error when running this line:
>> plt.title('Training: %i\n' % label, fontsize = 20)

Which gave this error:
plt.title('Training: %i\n' % label, fontsize = 20)
TypeError: %i format: a number is required, not str

Which, through the context given by the error we figured we would just cast the label as an integer:
>> plt.title('Training: %i\n' % int(label), fontsize = 20)

Then the code ran fine, and we checked that our output matched that of the webpage, and the only differences were that we have different misclassified digits and our accuracy was around 1% off.

Reasons this could be:
 Using the new import method for the dataset could have caused us to have different results. We could also have had differenct defaults regarding iteration rates from scikit-learn as the article could have used a different version than the current one, causing there to be a difference in the misclassified digits, and the accuracy level. While this is an assumption based off of warning we got about the convergence rate of the problem, we feel that there is not another bug, just a slight difference in methods due to the change in the data input and version changes in scikit-learn.

3)
In version 2.22 of scikit-learn the linear regression solver arguement's default changed from 'liblinear' to 'lbfgs', (found here:https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html), so the current default was used in this script.
The main difference between the two methods is the 'lbdfs' optimizes using a quasi-Newtonian method (BFGS: https://en.wikipedia.org/wiki/Broyden%E2%80%93Fletcher%E2%80%93Goldfarb%E2%80%93Shanno_algorithm), that is it approximates the hessian rather than computing it, and this method stores fewer updates, allowing it to take up less memory than 'liblinear'. Liblinear essentially performs a line search, and has problems that are spelled out well in this article: https://towardsdatascience.com/dont-sweat-the-solver-stuff-aea7cddc3451) while 'libnear' is a coordinate descent algorithm (explained here: https://towardsdatascience.com/dont-sweat-the-solver-stuff-aea7cddc3451).

4)
The difference between the two optimization methods above and the methods in the plot included in the "explanation" is summed up with:
its like comparing apples to an apple-orange hybrid. The methods in the plot, Stochastic Gradient Descent, NAG, Momentum, etc, are Optimizing Algorithms, just like BFGS and Coordinate Descent are, which means that their goal is to minimize the error of the training set over the coefficients of the logistic prediction function.  However, 'lbdgs' and 'liblinear' are python solver options for optimizing that same error as well as the optimality of method in context of memory usage and other aspects dealing with usage. The reasoning being this statement comes from this link: https://towardsdatascience.com/dont-sweat-the-solver-stuff-aea7cddc3451, which explains that lbfgs only stores the last few updates in memory which makes it more efficient in terms of memory, and that liblinear performs better with high dimensionality but is unable to run in parallel. In essence, we are saying that the two methods above are optimizing the minimization of the error in a manner that also impacts the efficiency of python's computation of that minimization, while the methods in the plot do not come with that much baggage as they are just optimizing with respect to the error. Overall, the pros and cons of using each solver method go further than the pros and cons of using one optimization algorithm versus the other (i.e. BFGS v coordinate descent which are algorithms, like the ones in the plots). Also neither coordinate descent nor BFGS are included in the plot. 

4) Ex)
The paths are spookily smooth, as if some form of smoother was applied to the paths for visualization purposes.

5)
There was one problem with the code for the confusion matrix using matplotlib, as xrange() was renamed to range() between Python 2 and Python 3. Otherwise, we were able to successfully implement the correlation matrix at the end of our file.

6)
The rows of the confusion matrix represent the actual label of the digit, and the columns represent the predicted label of the digit. Therefore, the matrix shows us the frequency missclassifications by label and predicted label (e.g. given the new input of a 0, what is the propoability of being misclassified as a 6).
The numbers being confused for 8s are:
5: frequency of 40
2: frequency of 43
3: frequency of 27
All digits were at some point confused for an eight, however, we just wanted to list the specific frequencies for three of the digits that were often confused for an 8.

8 was misclassified as:
3: 25 times
5: 22 times
9: 23 times
1: 17 times

8 was also misclassified as every other digit, but these were the most frequent.

7) This is included as a comment inour code, but it summarizes the features we added: 
This function creates 57 features by appending 57 other values to the image (i.e. observation in pixel activations). 28 of those features are the total number of 0s in a row of the image (which are 28 by 28s), this essentially captures the amount of "whitespace" in a row. 28 other features are the total sum of a row, which calculates the amount of ink laid down on a row, this is a complimentary measure to the "whitespace" features and are included in the data the same as the previous 28. 
The last added feature starts by classifying regions as empty and full on the criterium that empty is true empty and full is the inclusion of any activated pixels. Then we count the number of times in each row that the class changes from 1 to 0. This vector of 28 "flip counts" and count the number of times this vector starts decreasing along its entries. This returns one value per image in the data set, this value is meant to ecompass the number of holes in a given image of a digit, it is not a perfect but it acts as a proxy.
This definitely did not improve our detection of 8s, predicting 14 fewer 8s (even though we predicted 17 more 5s which was nice i guess). In theory, this is probably because messy 8s do not always have holes, as the loops can apear to be open so they were often missclassified as 2s. 


8) After including the new features, our accuracy increased from 0.9093 to 0.911 (not fantastic lol but we are talkin bout 8s right). And as I said before, we did not see an overall improvement.  Unfortanely the only digits whose confusion for 8s that marginally improved were 0 and 7. 
